
package top.dsbbs2.sr;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.ImporterTopLevel;
import org.mozilla.javascript.NativeFunction;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Scriptable;

import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.contact.User;
import top.dsbbs2.common.lambda.INoThrowsRunnable;

public class JSRunner
{
  public Context context;
  public Scriptable scope;
  public StringBuilder out=new StringBuilder();
  private volatile String code;
  private volatile Thread th;
  private volatile Throwable exc;
  private volatile Object ret;
  private final boolean unsafe;
  private final Contact con;
  private final User sender;
  
  public JSRunner()
  {
	  this(false,null,null);
  }
  
  public JSRunner(boolean unsafe,Contact con,User sender)
  {
	this.unsafe=unsafe;
	this.con=con;
	this.sender=sender;
    this.init();
  }

  public static Object runCode(final Context ctx, final Scriptable scope, final String c)
  {
      return ctx.evaluateString(scope, c, "main.js", 1, null);
  }
  @SuppressWarnings("deprecation")
  public Object eval(final String c)
  {
	  this.code=c;
	  long temp=System.nanoTime();
	  while(th.isAlive())
	  {
		  try {
			  Thread.sleep(1);
		  }catch (InterruptedException e) {
			// ignore
		}
		  if (!unsafe&&TimeUnit.NANOSECONDS.toSeconds(System.nanoTime()-temp)>=10) {
			while(th!=null&&th.isAlive()) th.stop();
			throw new RuntimeException("time out! limit: 10s");
		}
	  }
	  if(this.exc!=null)
		  INoThrowsRunnable.ct(this.exc);
	  return this.ret;
  }
  public void print(Object o)
  {
	  if(!unsafe&&out.length()+Objects.toString(o).length()>200)
		  throw new RuntimeException("output too long: limit: 200 actual: "+(out.length()+Objects.toString(o).length()));
	  out.append(o);
  }
  public void println(Object o)
  {
	  print(o+"\n");
  }
  @SuppressWarnings("serial")
  public void init()
  {
	  th=new Thread(()->{
		  try {
		      this.context = Context.enter();
		      this.context.setOptimizationLevel(-1);
		      this.context.setLanguageVersion(Context.VERSION_ES6);
		      if (!unsafe) {
		    	  this.scope = this.context.initSafeStandardObjects(null, true);
			  }else {
				  this.scope = new ImporterTopLevel(this.context, false);
				  this.scope.put("context", this.scope, new NativeJavaObject(this.scope, this.context, this.context.getClass()));
				  this.scope.put("scope", this.scope, new NativeJavaObject(this.scope,this.scope,this.scope.getClass()));
				  this.scope.put("contact", this.scope, new NativeJavaObject(this.scope,this.con,this.con.getClass()));
				  this.scope.put("sender", this.scope, new NativeJavaObject(this.scope,this.sender,this.sender.getClass()));
			  }
		      this.scope.put("print", this.scope, new NativeFunction() {
		  		
		  		@Override
		  		public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
		  			if(args.length>=1)
		  			JSRunner.this.print(args[0]);
		  			return null;
		  		}

		  		@Override
		  		protected String getParamOrVarName(int arg0) {
		  			return "";
		  		}
		  		
		  		@Override
		  		protected int getParamCount() {
		  			return 1;
		  		}
		  		
		  		@Override
		  		protected int getParamAndVarCount() {
		  			// TODO 自动生成的方法存根
		  			return 1;
		  		}
		  		
		  		@Override
		  		protected int getLanguageVersion() {
		  			// TODO 自动生成的方法存根
		  			return Context.VERSION_ES6;
		  		}
		  	});
		      this.scope.put("println", this.scope, new NativeFunction() {
		    		
		    		@Override
		    		public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
		    			if(args.length<1)
		    				args= new String[]{""};
		    			JSRunner.this.println(args[0]);
		    			return null;
		    		}

		    		@Override
		    		protected String getParamOrVarName(int arg0) {
		    			return "";
		    		}
		    		
		    		@Override
		    		protected int getParamCount() {
		    			return 1;
		    		}
		    		
		    		@Override
		    		protected int getParamAndVarCount() {
		    			// TODO 自动生成的方法存根
		    			return 1;
		    		}
		    		
		    		@Override
		    		protected int getLanguageVersion() {
		    			// TODO 自动生成的方法存根
		    			return Context.VERSION_ES6;
		    		}
		    	});
		    } catch (final Throwable e) {
		    	INoThrowsRunnable.ct(e);
		    }
		  while(JSRunner.this.code==null)
		  {
			  try {
			  Thread.sleep(1);
			  }catch (InterruptedException e) {
				// ignore
			}
		  }
		  JSRunner.this.ret=JSRunner.runCode(this.context, this.scope, JSRunner.this.code);
	  });
	  th.setDaemon(true);
	  th.setPriority(Thread.MIN_PRIORITY);
	  th.setUncaughtExceptionHandler((thr,exc)->{
		  JSRunner.this.exc=exc;
	  });
	  th.start();
  }

  @SuppressWarnings("deprecation")
  @Override
  protected void finalize() throws Throwable {
	while(th!=null&&th.isAlive()) th.stop();
	super.finalize();
  }

public static Object invokeJsFunction(final Function f, final Context ctx, final Scriptable scope,
      final Object... args)
  {
    return f.call(ctx, f, scope, args);
  }

  @SuppressWarnings("unchecked")
  public <T> T tryInvokeFunction(final String name, final Object... args)
  {
    final Object f = this.scope.get(name, this.scope);
    if (f instanceof Function) {
      return (T) JSRunner.invokeJsFunction((Function) f, this.context, this.scope, args);
    }
    return null;
  }
}
