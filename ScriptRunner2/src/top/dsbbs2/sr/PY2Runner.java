
package top.dsbbs2.sr;

import org.python.core.PyBuiltinFunction;
import org.python.core.PyJavaType;
import org.python.core.PyObject;
import org.python.core.PyType;
import org.python.util.PythonInterpreter;

import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.contact.User;

public class PY2Runner
{
  public final StringBuilder out=new StringBuilder();
  private final Contact con;
  private final User sender;

  public PY2Runner(Contact con,User sender)
  {
	this.con=con;
	this.sender=sender;
  }
  @SuppressWarnings("serial")
  public Object eval(final String c)
  {
	  try(PythonInterpreter interpreter=new PythonInterpreter()) {
		  interpreter.getLocals().__setitem__("q_print", new PyObject(PyType.fromClass(PyBuiltinFunction.class)) {

			@Override
			public PyObject __call__(PyObject arg0) {
				PY2Runner.this.print(arg0);
				return null;
			}
			  
		  });
		  interpreter.getLocals().__setitem__("q_println", new PyObject(PyType.fromClass(PyBuiltinFunction.class)) {

				@Override
				public PyObject __call__(PyObject arg0) {
					PY2Runner.this.println(arg0);
					return null;
				}
				  
			  });
		  interpreter.getLocals().__setitem__("contact",PyJavaType.wrapJavaObject(this.con));
		  interpreter.getLocals().__setitem__("sender",PyJavaType.wrapJavaObject(this.sender));
		  interpreter.getLocals().__setitem__("interpreter",PyJavaType.wrapJavaObject(interpreter));
		  return interpreter.eval(c);
	  }
  }
  public void print(Object o)
  {
	  out.append(o);
  }
  public void println(Object o)
  {
	  print(o+"\n");
  }
}
