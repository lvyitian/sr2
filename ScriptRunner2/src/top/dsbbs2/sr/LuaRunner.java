
package top.dsbbs2.sr;

import javax.script.ScriptException;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.script.LuaScriptEngine;
import org.luaj.vm2.script.LuaScriptEngineFactory;

import top.dsbbs2.common.lambda.INoThrowsRunnable;

public class LuaRunner
{
  public LuaScriptEngine engine;
  public final StringBuilder out=new StringBuilder();
  
  public LuaRunner()
  {
    this.init();
  }

  public Object eval(final String c)
  {
	  try {
	    return this.engine.eval(c);
      } catch (ScriptException e) {
	    INoThrowsRunnable.ct(e);
	    return null;
      }
  }
  public void print(Object o)
  {
	  out.append(o);
  }
  public void println(Object o)
  {
	  print(o+"\n");
  }

  public void init()
  {
	this.engine=(LuaScriptEngine) new LuaScriptEngineFactory().getScriptEngine();
	this.engine.put("q_print", new org.luaj.vm2.LuaFunction() {

		@Override
		public LuaValue call(LuaValue arg) {
			LuaRunner.this.print(arg);
			return null;
		}
		
	});
	this.engine.put("q_println", new org.luaj.vm2.LuaFunction() {

		@Override
		public LuaValue call(LuaValue arg) {
			LuaRunner.this.println(arg);
			return null;
		}
		
	});
  }
}
