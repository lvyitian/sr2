
package top.dsbbs2.sr;

import org.jruby.Ruby;
import org.jruby.RubyModule;
import org.jruby.internal.runtime.methods.DynamicMethod;
import org.jruby.javasupport.Java;
import org.jruby.runtime.Block;
import org.jruby.runtime.ThreadContext;
import org.jruby.runtime.builtin.IRubyObject;

import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.contact.User;

public class RubyRunner
{
  public final StringBuilder out=new StringBuilder();
  private final Contact con;
  private final User sender;
  public final Ruby ruby=Ruby.newInstance();

  public RubyRunner(Contact con,User sender)
  {
	this.con=con;
	this.sender=sender;
	ruby.loadJavaSupport();
	  ruby.setObjectSpaceEnabled(true);
	  ruby.getObject().addModuleFunction("q_print", new DynamicMethod("q_print") {
		
		@Override
		public DynamicMethod dup() {
			return this;
		}
		
		@Override
		public IRubyObject call(ThreadContext arg0, IRubyObject arg1, RubyModule arg2, String arg3, IRubyObject[] arg4,
				Block arg5) {
			if(arg4.length>=1)
			  RubyRunner.this.print(arg4[0]);
			return null;
		}
	});
	  ruby.getObject().addModuleFunction("q_println", new DynamicMethod("q_println") {
			
			@Override
			public DynamicMethod dup() {
				return this;
			}
			
			@Override
			public IRubyObject call(ThreadContext arg0, IRubyObject arg1, RubyModule arg2, String arg3, IRubyObject[] arg4,
					Block arg5) {
				if(arg4.length<1)
					arg4=new IRubyObject[] {Java.wrapJavaObject(RubyRunner.this.ruby, "")};
				  RubyRunner.this.println(arg4[0]);
				return null;
			}
		});
	  ruby.defineGlobalConstant("RubyVM", Java.wrapJavaObject(this.ruby, this.ruby));
	  ruby.defineGlobalConstant("Contact", Java.wrapJavaObject(this.ruby, this.con));
	  ruby.defineGlobalConstant("Sender", Java.wrapJavaObject(this.ruby, this.sender));
	  
  }

  public Object eval(final String c)
  {
	  return ruby.executeScript(c, "main.rb");
  }
  public void print(Object o)
  {
	  out.append(o);
  }
  public void println(Object o)
  {
	  print(o+"\n");
  }
}
