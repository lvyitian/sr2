package top.dsbbs2.sr;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Vector;

import net.mamoe.mirai.BotFactoryJvm;
import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.contact.User;
import net.mamoe.mirai.event.ListeningStatus;
import net.mamoe.mirai.event.internal.EventInternalJvmKt;
import net.mamoe.mirai.message.GroupMessageEvent;
import net.mamoe.mirai.message.MessageEvent;
import net.mamoe.mirai.utils.BotConfiguration;
import top.dsbbs2.common.lambda.IGenericAdvancedRunnable;
import top.dsbbs2.common.lambda.INoThrowsRunnable;

public class Main {
	public static final Vector<Thread> t=new Vector<>();
	public static boolean allowUnsafe(User u)
	{
		return /*true*/Objects.equals(u.getId(),535481388L)/*||Objects.equals(u.getId(), 1514009273L)*/;
	}
	public static void main(String[] args) {
		Global.INSTANCE.bot = BotFactoryJvm.newBot(Global.INSTANCE.config.getConfig().qq,
		          new String(Global.INSTANCE.config.getConfig().pass), new BotConfiguration()
		          {
		            {
		              this.fileBasedDeviceInfo("deviceInfo.json");
		            }
		          });
		Global.INSTANCE.bot.login();
		EventInternalJvmKt._subscribeEventForJaptOnly(MessageEvent.class, Global.INSTANCE.bot, e->{
			if (e.getMessage().contentToString().startsWith("run_js ")) {
				Contact con=e instanceof GroupMessageEvent?((GroupMessageEvent)e).getGroup():e.getSender();
				while(t.size()>4)
				{
					try {
						Thread.sleep(1);
					}catch (InterruptedException exc) {
						//ignore
					}
				}
				Thread temp=new Thread(()->{
					JSRunner r=new JSRunner(allowUnsafe(e.getSender()),con,e.getSender());
					Object ret=r.eval(e.getMessage().contentToString().substring(7));
					con.sendMessage(r.out.append("\n返回: "+Objects.toString(ret)).toString());
				});
				temp.setDaemon(true);
				//temp.setPriority(Thread.MIN_PRIORITY);
				temp.setUncaughtExceptionHandler((th,exc)->
					con.sendMessage("发现未处理异常: "+throwableToString(exc))
				);
				temp.start();
			}else if (allowUnsafe(e.getSender())&&e.getMessage().contentToString().startsWith("run_lua ")) {
				Contact con=e instanceof GroupMessageEvent?((GroupMessageEvent)e).getGroup():e.getSender();
				while(t.size()>4)
				{
					try {
						Thread.sleep(1);
					}catch (InterruptedException exc) {
						//ignore
					}
				}
				Thread temp=new Thread(()->{
					LuaRunner r=new LuaRunner();
					Object ret=r.eval(e.getMessage().contentToString().substring(8));
					con.sendMessage(r.out.append("\n返回: "+Objects.toString(ret)).toString());
				});
				temp.setDaemon(true);
				//temp.setPriority(Thread.MIN_PRIORITY);
				temp.setUncaughtExceptionHandler((th,exc)->
					con.sendMessage("发现未处理异常: "+throwableToString(exc))
				);
				temp.start();
			}else if (allowUnsafe(e.getSender())&&e.getMessage().contentToString().startsWith("run_py2 ")) {
				Contact con=e instanceof GroupMessageEvent?((GroupMessageEvent)e).getGroup():e.getSender();
				while(t.size()>4)
				{
					try {
						Thread.sleep(1);
					}catch (InterruptedException exc) {
						//ignore
					}
				}
				Thread temp=new Thread(()->{
					PY2Runner r=new PY2Runner(con,e.getSender());
					Object ret=r.eval(e.getMessage().contentToString().substring(8));
					con.sendMessage(r.out.append("\n返回: "+Objects.toString(ret)).toString());
				});
				temp.setDaemon(true);
				//temp.setPriority(Thread.MIN_PRIORITY);
				temp.setUncaughtExceptionHandler((th,exc)->
					con.sendMessage("发现未处理异常: "+throwableToString(exc))
				);
				temp.start();
			}else if (allowUnsafe(e.getSender())&&e.getMessage().contentToString().startsWith("run_ruby ")) {
				Contact con=e instanceof GroupMessageEvent?((GroupMessageEvent)e).getGroup():e.getSender();
				while(t.size()>4)
				{
					try {
						Thread.sleep(1);
					}catch (InterruptedException exc) {
						//ignore
					}
				}
				Thread temp=new Thread(()->{
					RubyRunner r=new RubyRunner(con,e.getSender());
					Object ret=r.eval(e.getMessage().contentToString().substring(9));
					con.sendMessage(r.out.append("\n返回: "+Objects.toString(ret)).toString());
				});
				temp.setDaemon(true);
				//temp.setPriority(Thread.MIN_PRIORITY);
				temp.setUncaughtExceptionHandler((th,exc)->
					con.sendMessage("发现未处理异常: "+throwableToString(exc))
				);
				temp.start();
			}else if (allowUnsafe(e.getSender())&&e.getMessage().contentToString().startsWith("run_php ")) {
				Contact con=e instanceof GroupMessageEvent?((GroupMessageEvent)e).getGroup():e.getSender();
				while(t.size()>4)
				{
					try {
						Thread.sleep(1);
					}catch (InterruptedException exc) {
						//ignore
					}
				}
				Thread temp=new Thread(()->{
					PHPRunner r=new PHPRunner(con,e.getSender());
					Object ret=r.eval(e.getMessage().contentToString().substring(8));
					try {
						r.env.flushAll();
					} catch (Throwable e1) {
						INoThrowsRunnable.ct(e1);
					}
					con.sendMessage(new StringBuilder(new String(r.out.toByteArray())).append("\n返回: "+Objects.toString(ret)).toString());
				});
				temp.setDaemon(true);
				//temp.setPriority(Thread.MIN_PRIORITY);
				temp.setUncaughtExceptionHandler((th,exc)->
					con.sendMessage("发现未处理异常: "+throwableToString(exc))
				);
				temp.start();
			}
			return ListeningStatus.LISTENING;
		});
		Global.INSTANCE.bot.join();
	}
	public static String throwableToString(Throwable e)
	{	
		java.io.ByteArrayOutputStream m=new java.io.ByteArrayOutputStream();
		java.io.PrintStream p=IGenericAdvancedRunnable.invoke(i->new java.io.PrintStream(m,true,"UTF8"), null);
		e.printStackTrace(p);
		return new String(m.toByteArray(),StandardCharsets.UTF_8);
	}
}
