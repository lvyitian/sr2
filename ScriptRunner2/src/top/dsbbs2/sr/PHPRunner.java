
package top.dsbbs2.sr;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.util.Objects;

import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.contact.User;
import php.runtime.Memory;
import php.runtime.env.Environment;
import top.dsbbs2.common.lambda.INoThrowsRunnable;

public class PHPRunner
{
  public final ByteArrayOutputStream out=new ByteArrayOutputStream();
  public final Environment env=new Environment(new PrintStream(this.out,true));
  public final Contact contact;
  public final User sender;
  public PHPRunner(Contact contact,User sender) {
	  this.contact=contact;
	  this.sender=sender;
	  synchronized (PHPInfo.lock) {
		  long tmp=PHPInfo.cid.getAndIncrement();
		  PHPInfo.objs.put(tmp, new WeakReference<>(this));
		  this.env.defineConstant("RunnerID", Memory.wrap(this.env, tmp), true);
	  }
  }

  public Object eval(final String c)
  {
	  try {
	    return this.env.eval(c);
      } catch (Throwable e) {
	    INoThrowsRunnable.ct(e);
	    return null;
      }
  }
  public void print(Object o)
  {
	  try {
		out.write(Objects.toString(o).getBytes());
	} catch (IOException e) {
		throw new RuntimeException(e);
	}
  }
  public void println(Object o)
  {
	  print(o+"\n");
  }
}
