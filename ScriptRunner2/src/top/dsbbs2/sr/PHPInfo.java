package top.dsbbs2.sr;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PHPInfo {
	public static final AtomicLong cid=new AtomicLong(0);
	public static final ConcurrentHashMap<Long, WeakReference<PHPRunner>> objs=new ConcurrentHashMap<>();
	public static final Object lock=new Object();
}
